package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class ImageSerializerBase64Impl implements ImageSerializer {

	/**
	 * La méthode serialize permet d'encoder un fichier image en ASCII
	 */
	public String serialize(File image) {
		byte[] fileContent = null;
		try {
			fileContent = Files.readAllBytes(image.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		String encodedString = Base64.getEncoder().encodeToString(fileContent);
		return encodedString;
	}

	/**
	 * La méthode deserialize permet de décoder une chaine de charactère contenant les données d'une image
	 */
	public byte[] deserialize(String encodedImage) {
		byte[] decodedBytes = Base64.getDecoder().decode(encodedImage);
		return decodedBytes;
	}

}
