package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;

/**
 * Interface pour sérialiser et désérialiser un fichier image
 */

public interface ImageSerializer {

	/**
	 * Méthode pour sérialiser l'image
	 * @param image le fichier image
	 * @return encodedString String contenant les données de l'image encodée
	 */
	String serialize(File image);

	/**$
	 * Méthode pour désérialiser l'image
	 * @param encodedImage String contenant les données de l'image encodée
	 * @return decodedBytes Bytes décodées
	 */
	byte[] deserialize(String encodedImage);

}
